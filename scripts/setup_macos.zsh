#!/usr/bin/env zsh

echo "\n<<< Starting macOS Setup >>>\n"

# Finder > View > Show Path Bar
defaults write com.apple.finder ShowPathbar -bool true